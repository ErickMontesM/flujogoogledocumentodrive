package pages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.webdriver.WebDriverFacade;

public class DocumentoWord extends PageObject {

	@FindBy(id = "identifierNext")
	private WebElementFacade botonSiguiente;

	@FindBy(id = "identifierId")
	private WebElementFacade cajaTextoCorreoElectronico;

	@FindBy(xpath = "//*[@id=\"gbwa\"]/div[1]/a")
	private WebElementFacade iconoOpciones;

	@FindBy(css = "#yDmH0d > c-wiz > div > div > c-wiz > div > div > ul:nth-child(3)")
	private WebElementFacade opciones;

	@FindBy(linkText = "Ir a Google Drive")
	private WebElementFacade botonlinkGoogleDrive;

	@FindBy(name = "password")
	private WebElementFacade cajaTextoContraseña;

	@FindBy(id = "aso_search_form_anchor")
	private WebElementFacade EsperaVisibleCajaTextoBuscador;

	@FindBy(xpath = "//*[@id=\"drive_main_page\"]/div[2]/div[1]/div[1]/div/div/div[2]/div[1]/div/button[1]")
	private WebElementFacade botonMasNuevo;
	
	@FindBy(className="docs-title-input")
	private WebElementFacade textoNombreDocumento;

	public void getUrl() {
		getAllWebDriver().get("https://www.google.com/");
	}

	public void IconoDesplegable() {
		WebDriverWait wait = new WebDriverWait(getAllWebDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(iconoOpciones));
		iconoOpciones.click();
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void seleccionarOpcion(String opcion) {
		WebElement ele = getAllWebDriver().findElement(By.xpath("//iframe[contains(@id,'I0')]"));
		getAllWebDriver().switchTo().frame(ele);

		List<WebElement> li = opciones.findElements(By.xpath("li/a/span"));

		for (int i = 0; i < li.size(); i++) {
			if (li.get(i).getText().equals(opcion)) {
				li.get(i).click();
				System.out.println("Si ok");
				break;
			}
		}
	}

	public void ingresoCredenciales(String usuario, String contraseña) {

		WebDriverWait wait = new WebDriverWait(getAllWebDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(botonlinkGoogleDrive));

		botonlinkGoogleDrive.click();
		wait.until(ExpectedConditions.visibilityOf(cajaTextoCorreoElectronico));
		cajaTextoCorreoElectronico.sendKeys(usuario);
		botonSiguiente.click();
		wait.until(ExpectedConditions.elementToBeClickable(cajaTextoContraseña));
		cajaTextoContraseña.sendKeys(contraseña, Keys.ENTER);
	}

	public void creacionDocumento() {
		WebDriverWait wait = new WebDriverWait(getAllWebDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(EsperaVisibleCajaTextoBuscador));
		botonMasNuevo.click();
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		botonMasNuevo.sendKeys(Keys.DOWN);
		botonMasNuevo.sendKeys(Keys.DOWN);
		botonMasNuevo.sendKeys(Keys.DOWN);
		botonMasNuevo.sendKeys(Keys.ENTER);
		botonMasNuevo.sendKeys(Keys.ENTER);
//		((JavascriptExecutor) getAllWebDriver()).executeScript("window.open()");
		ArrayList<String> newTab = new ArrayList<String>(getAllWebDriver().getWindowHandles());
		getAllWebDriver().switchTo().window(newTab.get(1));
	}
	
	public void nombrarDocumento(String nombre) {
		WebDriverWait wait = new WebDriverWait(getAllWebDriver(), 20);
		wait.until(ExpectedConditions.elementToBeClickable(textoNombreDocumento));
		textoNombreDocumento.click();
		textoNombreDocumento.clear();
		Date myDate = new Date();
		String fecha=new SimpleDateFormat("dd-MM-yyyy").format(myDate);
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		 Date date = new Date();
		String hora=dateFormat.format(date);
		textoNombreDocumento.sendKeys("E01_"+nombre+"_"+fecha+" "+hora);
	}

	private WebDriver getAllWebDriver() {
		WebDriverFacade facade = (WebDriverFacade) getDriver();
		return facade.getProxiedDriver();
	}

}
