package features;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = { "json:target/sire/serenity/cucumber.json" }, features = {
		"src//test//resources//features//DocumentoWord.feature" })
public class RunnerTest {

}
