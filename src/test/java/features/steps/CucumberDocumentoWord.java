package features.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import features.steps.serenity.StepDocumentoWord;
import net.thucydides.core.annotations.Steps;

public class CucumberDocumentoWord {

	@Steps
	StepDocumentoWord stepHome;
	
	@Given("^ingresamos al buscador google$")
	public void ingresamos_al_buscador_google() {
		stepHome.getUrl();
	}
	
	@And("^nos dirigimos al menu de Google Apps$")
	public void nos_dirigimos_al_menu_de_google_apps() {
	stepHome.IconoDesplegable();
	}
	
	@And("^seleccionamos (.*)$")
	public void seleccionamos_opcion(String opcion) {
	stepHome.seleccionarOpcion(opcion);	
	}
	
	@When("^accedemos a la cuenta de google con (.*) y (.*)$")
	public void accedemos_a_la_cuenta_de_google_con(String usuario,String contraseña) {
	stepHome.ingresoCredenciales(usuario, contraseña);
	}
	
	@Then("^creamos un documento en blanco$")
	public void creamos_un_documento_en_blanco() {
		stepHome.creacionDocumento();
	}
	
	@And("^nombramos al documento con (.*) y fecha y hora exacta de la prueba$")
	public void nombramos_al_documento_con_y_fecha_y_hora_exacta_de_la_prueba(String nombre){
		stepHome.nombrarDocumento(nombre);
	}
}
