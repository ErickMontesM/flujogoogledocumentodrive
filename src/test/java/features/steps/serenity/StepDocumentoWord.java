package features.steps.serenity;

import net.thucydides.core.annotations.Step;
import pages.DocumentoWord;

public class StepDocumentoWord {
	DocumentoWord pageH;
	
	@Step
	public void getUrl() {
		pageH.getUrl();
	}
	
	@Step
	public void IconoDesplegable() {
		pageH.IconoDesplegable();
	}
	
	@Step
	public void seleccionarOpcion(String opcion) {
		pageH.seleccionarOpcion(opcion);
	}
	
	@Step
	public void ingresoCredenciales(String usuario,String contraseña) {
		pageH.ingresoCredenciales(usuario, contraseña);
	}
	
	@Step
	public void creacionDocumento() {
		pageH.creacionDocumento();
	}
	
	@Step
	public void nombrarDocumento(String nombre) {
		pageH.nombrarDocumento(nombre);
	}
	
	
	
	
}
