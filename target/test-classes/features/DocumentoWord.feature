Feature: Creamos Documento Word

Scenario Outline: Creacion de documento Word
Given ingresamos al buscador google
And nos dirigimos al menu de Google Apps
And seleccionamos <opcion>
When accedemos a la cuenta de google con <usuario> y <contraseña>
Then creamos un documento en blanco
And nombramos al documento con <nombre> y fecha y hora exacta de la prueba
Examples:
|opcion|usuario|contraseña|nombre|
|Drive|prueba.multiplica0814@gmail.com|Pruebamultiplic@0814|Erick Montes Morales|